import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CalculatorGUIService {

  constructor(private http: HttpClient) { }

  getAnswer(polynomial1: string, polynomial2: string, operation: string): Observable<string> {
    let params = new HttpParams;
    params = params.append('polynomial1', polynomial1);
    params = params.append('polynomial2', polynomial2);
    params = params.append('operation', operation);
    return this.http.get<string>('http://localhost:8080/api/calculator', {params: params});
  }

}
