import { TestBed } from '@angular/core/testing';

import { CalculatorGUIService } from './calculator-gui.service';

describe('CalculatorGUIService', () => {
  let service: CalculatorGUIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculatorGUIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
