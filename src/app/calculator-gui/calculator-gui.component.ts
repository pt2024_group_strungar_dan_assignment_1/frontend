import { Component, OnInit } from '@angular/core';
import { CalculatorGUIService } from '../services/calculator-gui.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-calculator-gui',
  templateUrl: './calculator-gui.component.html',
  styleUrls: ['./calculator-gui.component.css']
})
export class CalculatorGUIComponent implements OnInit{

  constructor(private cs: CalculatorGUIService,
              private formBuilder: FormBuilder) {  
  }

  titleState: string = 'in';
  fadeInComplete: boolean = false;

  displayAnswer: boolean = false;
  answer: string = '';
  polynomialForm!: FormGroup;
  errorMessage: string = '';

  ngOnInit(): void {
    this.polynomialForm = this.formBuilder.group({
      polynomial1: ['', [Validators.required, this.polynomialValidator]],
      polynomial2: ['', [Validators.required, this.polynomialValidator]],
      operation: ['', [Validators.required, this.operationValidator]]
    });
  }
  
  clearForm() {
    this.polynomialForm.reset();
    this.polynomialForm.markAsPristine();
    this.polynomialForm.markAsUntouched();
    this.displayAnswer = false;
    this.errorMessage = ''; // Clear error message
  }
  
  loadPolynomial(polynomial1: string, polynomial2: string, operation: string): void{
    this.cs.getAnswer(polynomial1, polynomial2, operation)
      .subscribe(
        (data: string) => {
          this.answer = data;
          console.log(data);
          if(this.answer != ''){
            this.displayAnswer = true;
            this.answer = this.answer.replace('[', '').replace(']', '');
            console.log(this.answer);
          }
        }
      );
  }

  calculate() {
    if(this.polynomialForm.invalid){
      this.errorMessage = 'Invalid input';
      return;
    }
    console.log(this.polynomialForm.value);
    const { polynomial1, polynomial2, operation } = this.polynomialForm.value;
    this.loadPolynomial(polynomial1, polynomial2, operation);
  }

  polynomialValidator(control: AbstractControl): { [key: string]: boolean } | null {
    let polynomial = control.value;
    console.log(polynomial);
    if(polynomial == '' || polynomial == null){
      return null;
    }
    let aux = polynomial;
    aux = aux.replace(/\s/g, '');
    aux = aux.replace(/([-+]?\d*x\^\d+)|([-+]?(?<=[-+])\d+)|([-+]?\d*x)|(^\d+)/g, ''); 
    console.log(aux);
    if (aux != '') {
      console.log('invalid polynomial');
      return {'invalidPolynomial': true};
    }
    return null;
  }
  operationValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      console.log('no operation selected');
      return {'noOperationSelected': true};
    }
    return null;
  }
  shouldDisplaySecondPolynomial(): boolean {
    const operation = this.polynomialForm.get('operation')?.value;
    return operation === 'addition' || operation === 'subtraction' || operation === 'multiplication' || operation === 'division' || operation === 'remainder' || operation === 'quotient' || operation === 'derivative' || operation === 'integral' || operation === 'definiteIntegral' || operation === '' || operation === null;
}
}