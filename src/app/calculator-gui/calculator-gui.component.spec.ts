import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorGUIComponent } from './calculator-gui.component';

describe('CalculatorGUIComponent', () => {
  let component: CalculatorGUIComponent;
  let fixture: ComponentFixture<CalculatorGUIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CalculatorGUIComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CalculatorGUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
